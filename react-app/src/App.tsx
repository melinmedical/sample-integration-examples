import React from 'react';
import logo from './logo.svg';

import {loginService, LoginView} from '@psky/signin-integration';
import {HttpApplicationContext, HttpSession, sessionFactory} from '@psky/system-integration';
import SystemIntegration from "./SystemIntegration";

const integrationUserName = "xkzyEKEoXRrUhIs";
const integrationUserPassword = "T4r1VWchc#dokp&";
const integrationTenantId = "69860b22-b873-4810-918c-dfba41dee95e";


const App = () =>{
  loginService.initialize({
    environment: "development",
    postLoginRedirectUrl: "http://localhost:3000/",
    loginMode: "servicePractitioner",
    countryCode: "NO",
    onSessionExpired: () => {
      alert("session has expired");
      window.location.href = "/";
    },
    getSessionRefreshToken: () => {
      return localStorage.getItem("sessionRefreshToken");
    },
  });

  const params = new URLSearchParams(window.location.search);

  // Setup state. If the page has the login session id we have been redirected back
  // and is then not using mocks
  const [userSession, setUserSession] = React.useState<HttpSession>();
  const [applicationContext, setApplicationContext] = React.useState<HttpApplicationContext>();


  // When the login component sends us a redirect back complete the login and acquire sessions
  React.useEffect(() => {

        if (params.has("loginSessionId")) {
          const loginSessionId = params.get("loginSessionId");
          void handleSignInCallback(loginSessionId);
        }
      },
      []
  );

  // Clear session between changing between mocks and real login
  const clearSession = () => {
    // @ts-ignore
    setApplicationContext();
    // @ts-ignore
    setUserSession(null);
  };


  const initialize =  (newUserSession: HttpSession, newIntegrationSession: HttpSession) => {

    newIntegrationSession.createApplicationContext(integrationTenantId).then(response => {
      setApplicationContext(response);
      setUserSession(newUserSession);
    })

  };

  // Handle the sign in after we receive a callback
  const handleSignInCallback = (loginSessionId: string | null) => {
    if (userSession != null) {
      // We are already signed in
      return;
    }
    if (loginSessionId != null) {

      loginService.handleLogin(
          loginSessionId,
          {username: integrationUserName, password: integrationUserPassword},
          (sessionRefreshToken) => {
            localStorage.setItem("sessionRefreshToken", sessionRefreshToken);
          }
      ).then(result => {
        const newUserSession:HttpSession = result;
        sessionFactory.authenticate(integrationUserName, integrationUserPassword).then(newIntegrationSession => {
          initialize(newUserSession,newIntegrationSession)
        })
      })
    }
  };


  // Render full page
  const renderPage = (isAuthenticated:any) => {
    if (isAuthenticated)
      return <SystemIntegration context={ applicationContext }  />
    else
      return <LoginView/>
  }

  const content = renderPage(userSession);


  return (
      <div className="min-w-screen min-h-screen p-5 bg-blue-100 flex items-center">
        <div className="border rounded w-2/3 py-8 px-4 mx-auto my-auto bg-white">
          <img src={logo} alt="logo" className="h-10 mx-auto mb-2" />
          <h2 className="uppercase font-weight-bolder mb-2 text-lg text-center">Hello World Application</h2>
          <button onClick={clearSession}>Logout</button>
          <hr />
          <div className="p-4 border mt-4">
            { content }
          </div>
        </div>
      </div>
  );
}

export default App;
