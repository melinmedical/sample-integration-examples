import React, {useEffect} from "react";
import {Platform, SystemFeatures} from "@psky/integration-sdk";

// @ts-ignore
const SystemIntegration = ({ context })=> {
    const [serviceCategories, setServiceCategores] = React.useState<Platform.ServiceCategory[]>([]);

    useEffect(()=> {
      listCategories()
    },[])

    // Use service feature to query for service categories
    const listCategories = () => {
        // @ts-ignore
        SystemFeatures.Global.Read.ServiceCategories.V1.all.request(context, new Platform.Empty()).then(result => {
            if (!result.success) {
                console.log("Ouch: " + result.error.code + ", " + result.error.description + ", " + result.error.technicalDescription);
                return;
            }
            setServiceCategores(result.value)
        });

    };

    return(
        <>
            <h4 className="uppercase font-weight-bolder mb-2 text-lg text-center text-blue-900">Service Categories</h4>
            <p className="mb-3 flex flex-col">
                { serviceCategories
                    .map(c => { return <span className="mb-2 w-2/3 mx-auto text-center border rounded p-2"> {c.id} - {c.name}</span>; }) }
            </p>
        </>

    );
};

export default SystemIntegration