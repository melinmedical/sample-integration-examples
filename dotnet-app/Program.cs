﻿using System;
using System.Threading.Tasks;
using PatientSky.Core;
using PatientSky.SDK;
using PatientSky.SDK.Communication;
using PatientSky.SDK.Core;
using PlatformIdentifier = PatientSky.SDK.Identifiers.PlatformIdentifier;


namespace sample_integration_net
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () =>
            {
                var session = await Platform
                    .Environment("https://psno-sandbox-partnergateway.patientsky.dev", "ps-partner-test>sandbox")
                    .OnLogMessage(Console.WriteLine)
                    .OnCredentialsRequest(() => new CredentialsInsecure("8MTqOajgsMlNrEP", "qCq$J8*$3cEH!m7"))
                    .Connect();
                
                var tenantId = Guid.Parse("69860b22-b873-4810-918c-dfba41dee95e");
                var healthcareConsumer = Guid.Parse("28be21da-5742-11eb-9ca2-4e671c867bbe");
                var serviceProvider = Guid.Parse("2a0745ca-67ab-11eb-abd9-66a6447826dd");


                // var context1 = session.CreateApplicationContext(tenantId);
                //
                // var serviceCategoryResponse =
                //     await context1.Request(SystemFeatures.global.read.serviceCategories.v1.all);
                // if (!serviceCategoryResponse.Success)
                // {
                //     Console.WriteLine("Failed to fetch service categories {0} {1}", serviceCategoryResponse.Error.Code,
                //         serviceCategoryResponse.Error.TechnicalError);
                //     return;
                // }
                //
                // Console.WriteLine("Service Categories List:");
                // foreach (var serviceCategory in serviceCategoryResponse.Value)
                // {
                //     Console.WriteLine("\t{0} - {1}", serviceCategory.id, serviceCategory.name);
                // }
                
                
                var context = session.CreateHealthcareConsumerContext(tenantId, healthcareConsumer);
                var contextWithProvider = context.WithProvider(serviceProvider);
                
                var prep= await contextWithProvider
                    .Request(SystemFeatures.communication.update.communicationItems.healthcareConsumer.v1.prepare,
                        new PrepareForHealthcareConsumer() { systemTypeId = "PATIENT_ENQUIRY" });
                exitError(prep);
                
                var res = await contextWithProvider
                    .Request(
                        SystemFeatures.communication.write.communicationItems.healthcareConsumer.v1.create,
                        
                        new CreateCommunicationItemAsHealthcareConsumer() {
                            flowId = prep.Value.data.flowId,
                            subject = "Test",
                            message = new Message() {
                                contextualName = new ContextualName() {
                                    context = new ContextualIdentifierData[] {
                                        new ContextualIdentifierData(
                                            "name",
                                            new PlatformIdentifier() { id = "identifier", terminology = "terminology" })
                                    },
                                    name = "contextual_name"
                                },
                                displayName = "displayName",
                                text = "text"
                            },
                            receivers = new PlatformIdentifier[] { 
                                new PlatformIdentifier() { // John Ronald Tolkien
                                    id = "0e423c70-5679-11eb-93fe-8a50d409bd22",
                                    terminology = "reference.actor.serviceProvider.position"
                                }
                            }
                        });
                exitError(res);

                // var readResponse = await contextWithProvider
                //     .Request(
                //         SystemFeatures.communication.update.communicationItems.healthcareConsumer.v1.markAsRead,
                //         new GuidId() { id = res.Value.id });
                // exitError(readResponse);
                //
                // var completeResponse = await contextWithProvider
                //     .Request(
                //         SystemFeatures.communication.update.communicationItems.healthcareConsumer.v1.complete,
                //         new CompleteCommunicationItem() { id = res.Value.id});
                // exitError(completeResponse);
                
                var searchResponse = await contextWithProvider
                    .Request(
                        SystemFeatures.communication.read.communicationItems.healthcareConsumer.v1.search,
                        new SearchHealthcareConsumerCommunications()
                        {
                            ids = new Guid[]{ healthcareConsumer, },
                            limit = 10,
                            offset = 0
                        }
                    );
                exitError(searchResponse);
            }).Wait();
        }
        static void exitError(Response res)
        {
            if (!res.Success)
            {
                Console.WriteLine("Failed {0} {1}", res.Error.Code, res.Error.TechnicalError);
                return;
            }
        }
    }
}
