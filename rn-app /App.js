/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import {Platform, SystemFeatures} from '@psky/integration-sdk';
import {loginService, LoginView} from '@psky/signin-integration-rn';
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';
import {
  Colors
} from 'react-native/Libraries/NewAppScreen';

const Section = ({title}) => {
    const [result, setResult] = useState<Platform.ServiceCategory[]>(null);
    return (
        <View style={styles.sectionContainer}>
            <Text
                style={[
                    styles.sectionTitle,
                    {
                        color: Colors.black,
                    },
                ]}>
                {title}
            </Text>
            {loginService.config && !result ? (
                <View style={{width: '90%'}}>
                <LoginView
                    onError={err => {
                        console.log(err);
                    }}
                    onSuccess={async sessionId => {
                        const session = await loginService.handleLogin(
                            sessionId,
                            {
                                username: '8MTqOajgsMlNrEP',
                                password: 'qCq$J8*$3cEH!m7',
                            },
                            sessionRefreshToken => {
                                console.log('sessionRefreshToken', sessionRefreshToken);
                                // AsyncStorage.setItem(
                                //   'sessionRefreshToken',
                                //   sessionRefreshToken,
                                // );
                            },
                        ).catch((e)=>console.log("login error", JSON.stringify(e)));

                        const context = await session.createHealthcareConsumerContext(
                            '69860b22-b873-4810-918c-dfba41dee95e',
                            '28be21da-5742-11eb-9ca2-4e671c867bbe',
                        ).catch((e)=>console.log("context error", e));

                        const result =
                            await SystemFeatures.Global.Read.ServiceCategories.V1.all.request(
                                context,
                                {},
                            ).catch((e)=>console.log("sys fet error", e));

                        setResult(result.value);
                    }}
                />
                </View>
            ) : (
                {result} ?
                    renderCategories(result)
                    :
                <Text style={{color: Colors.red, fontSize: 16}}>{'Fetching...'}</Text>
            )}
        </View>
    );
};

const renderCategories = (res: any)=>{
    console.log("service categories", res)
    return (
        <Text style={{color: Colors.red, fontSize: 16}}>{JSON.stringify(res, null, 2)}</Text>
    )

}

const App = () => {
    const [loading, setLoading] = useState(true);

    const load = async () => {
        await loginService.initialize({
            environment: 'psno-sandbox',
            postLoginRedirectUrl: 'http://localhost:3000/home',
            loginMode: 'patientApp',
            countryCode: 'NO',
            onSessionExpired: () => {
                alert('session has expired');
                window.location.href = '/';
            },
            getSessionRefreshToken: async () => {
                await AsyncStorage.getItem('sessionRefreshToken');
            },
        });

        setLoading(false);
    };
    useEffect(() => {
        load();
    }, []);

  // const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: Colors.lighter,
  };

  return (
      <SafeAreaView style={backgroundStyle}>
        <StatusBar barStyle={'light-content'} />
        <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={backgroundStyle}>
            <View
                style={{
                    backgroundColor: Colors.white,
                }}>
            {!loading && (
                <Section title="PS LOGIN"/>
            )}
            </View>
        </ScrollView>
      </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
